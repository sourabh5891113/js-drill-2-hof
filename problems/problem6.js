let getSalariesBasedOnCountry = require("./problem5");

function getAverageSalaryBasedOnCountry(data) {
  if (Array.isArray(data)) {
    let salariesWRTcountry = getSalariesBasedOnCountry(data);
    let countriesCount = data.reduce((acc,person)=>{
      if (acc[person.location]) {
        acc[person.location] = acc[person.location] + 1;
      } else {
        acc[person.location] = 1;
      }
      return acc;
    },{});

    for (let key in salariesWRTcountry) {
      salariesWRTcountry[key] = salariesWRTcountry[key] / countriesCount[key];
      salariesWRTcountry[key] = Math.floor(salariesWRTcountry[key] * 100) / 100;
    }
    console.log(salariesWRTcountry);
    return salariesWRTcountry;
  } else {
    console.log("First agrument should be an array");
    return {};
  }
}

module.exports = getAverageSalaryBasedOnCountry;
