let convertSalaryIntoNumbers = require("./problem2");

function getSalariesBasedOnCountry(data) {
  if (Array.isArray(data)) {
    convertSalaryIntoNumbers(data);
    let salariesBasedOnCountry = data.reduce((acc,person)=>{
      if (acc[person.location]) {
        acc[person.location] = acc[person.location] + person.salary;
        acc[person.location] = Math.floor(acc[person.location] * 100) / 100;
      } else {
        acc[person.location] = person.salary;
      }
      return acc;
    },{});

    return salariesBasedOnCountry;
  } else {
    console.log("First agrument should be an array");
    return {};
  }
}

module.exports = getSalariesBasedOnCountry;
