function findAllWebDevelopers(data) {
  if (Array.isArray(data)) {
    let webDevelpers = data.filter((person) => {
      return person.job.includes("Web Developer");
    });
    console.log(webDevelpers);
  } else {
    console.log("First agrument should be an array");
  }
}

module.exports = findAllWebDevelopers;
