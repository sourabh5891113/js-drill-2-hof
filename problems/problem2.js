function convertSalaryIntoNumbers(data) {
  if (Array.isArray(data)) {
    let newData = data.map((person) => {
      person.salary = Number(person.salary.substring(1));
      return person;
    });
    return newData;
  } else {
    console.log("First agrument should be an array");
    return [];
  }
}

module.exports = convertSalaryIntoNumbers;
