let convertSalaryIntoNumbers = require("./problem2");

function addCorrectedSalaryToData(data) {
  if (Array.isArray(data)) {
    let convertedData = convertSalaryIntoNumbers(data);
    convertedData = convertedData.map((person) => {
      person.corrected_salary = person.salary * 10000;
      return person;
    });
    return convertedData;
  } else {
    console.log("First agrument should be an array");
    return [];
  }
}

module.exports = addCorrectedSalaryToData;
