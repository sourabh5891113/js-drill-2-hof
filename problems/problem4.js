let convertSalaryIntoNumbers = require("./problem2");

function getSalariesSum(data) {
  if (Array.isArray(data)) {
    let convertedSalaryData = convertSalaryIntoNumbers(data);
    let salariesSum = convertedSalaryData.reduce((acc, person) => {
      return acc + person.salary;
    }, 0);
    console.log(salariesSum);
  } else {
    console.log("First agrument should be an array");
    return [];
  }
}

module.exports = getSalariesSum;
